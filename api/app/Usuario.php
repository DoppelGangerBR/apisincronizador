<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable  = ['id','usuario','senha'];
    protected $hidden = [];
    protected $connection = 'alfameta';
    protected $table = 'usuariosweb';

    public function usuario(){
        return $this->hasMany('App\Usuario');
    }
    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }
}
