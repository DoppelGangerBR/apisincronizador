<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SincronizacaoController extends Controller
{
    /*
    ==========================================================================================================
    */
    public function retornaClientes()
    {         
        $clientes = DB::connection('alfameta')->select('SELECT nome,
            endereco,
            clientedesde,
            telefone,
            sonumeros(cnpj_cpf) as cnpj_cpf,
            email, 
            datanascimento, 
            codclienteindicou, 
            sexo, 
            celular, 
            diavencimento, 
            valormensalidade, 
            diascarenciacobranca,
            datacobrancaprogramadocliente,
            numerocontrato,
            datainiciocontrato FROM Cliente'
         );

        array_walk_recursive($clientes, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($clientes);
    }
    /*
    ==========================================================================================================
    */
    public function retornaDependentes(){
        $dependentes = DB::connection('alfameta')->select(
            'select du.cpf,
            d.nome,
            d.datahorainclusao,
            d.codcliente,
            d.codgrauparentesco,
            du.coddependente,
            d.datahoraalteracao,
            d.ativo,
            d.statuscadastro from DependenteCliente as d 
            join(select max(CodDependente),cpf 
            from DependenteCliente group by cpf) as du( CodDependente,CPF) on(d.CodDependente = DU.CodDependente);
            SELECT * FROM #DependenteCliente;'
        );
        array_walk_recursive($dependentes, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($dependentes);
    }
    /*
    ==========================================================================================================
    */
    public function retornaClinicas(){
        $clinicas = DB::connection('alfameta')->select(
            'select
            codigo,
            nome,
            endereco,
            bairro,
            numero,
            telefone,
            celular,
            tipoConvenio from CadastroClinica'
        );

        array_walk_recursive($clinicas, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($clinicas);
    }
    /*
    ==========================================================================================================
    */
    public function retornaCidades(){
        $cidades = DB::connection('alfameta')->select(
            'select codmunicipio, nome, nomecompleto, uf from municipio'
        );
        array_walk_recursive($cidades, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($cidades);
    }
    /*
    ==========================================================================================================
    */
    public function retornaMedicos(){
        $medicos = DB::connection('alfameta')->select(
            'select codigo,
            Nome,
            crm,
            telefone,
            valorconsulta,
            valorconsultaconvenio,
            celular from CadastroConveniado'
        );
        array_walk_recursive($medicos, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($medicos);

    }
    /*
    ==========================================================================================================
    */
    public function retornaServicos(){
        $servicos = DB::connection('alfameta')->select(
            'SELECT
            codigo,
            nome,
            valor, 
            percentualDesconto, 
            valorConvenio from CadastroServicoConveniado'
        );
        array_walk_recursive($servicos, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($servicos);
    }
    /*
    ==========================================================================================================
    */
    public function historicoPagamentoUsuario()
    {   
        $historico = DB::connection('alfameta')->select('BEGIN 
            SELECT rt.codEmpresa, 
                    rt.codrecebimento, 
                    rt.codcontareceber, 
                    r.datarecebimento, 
                    cr.codcliente, 
                    rt.valorrecebimentoliquido, 
                    rank(*) over(PARTITION BY CR.CodCLiente ORDER BY r.datarecebimento DESC) AS rank 
                INTO #Receb
                FROM DBA.RecebimentoTitulo  AS rt 
                JOIN DBA.Recebimento AS R ON (rt.codempresa = r.codempresa AND rt.codrecebimento = r.codrecebimento) 
                JOIN DBA.ContaReceber AS CR ON (rt.codempresa = CR.codempresa AND rt.codcontareceber = CR.codcontareceber) 
                WHERE r.status = \'R\';
            DELETE FROM #Receb WHERE rank >= 10;
            
            SELECT codEmpresa, codrecebimento, codcontareceber, datarecebimento, codcliente, valorrecebimentoliquido FROM #Receb             
        end'); 
    
        array_walk_recursive($historico, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($historico);
    }  
    /*
    ==========================================================================================================
    */
    public function retornaMedicoXClinica(){
        $medicoXClinica = DB::select(
            'SELECT * FROM CadastroConveniadoXCadastroClinica'
        );
        array_walk_recursive($medicoXClinica, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });

        return response()->json($medicoXClinica);
    }
    /*
    ==========================================================================================================
    */
    public function retornaMedicoXServico(){
        $medicoXServico = DB::select('
            SELECT * FROM CadastroConveniadoXCadastroServicoConveniado
        ');
        array_walk_recursive($medicoXServico, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($medicoXServico);
    }
    /*
    ==========================================================================================================
    */        
    public function retornaClinicaXServico(){
        $clinicaXServico = DB::connection('alfameta')->select(
            'SELECT * FROM CadastroClinicaXCadastroServicoConveniado'
        );
        array_walk_recursive($clinicaXServico, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($clinicaXServico);
    }
    /*
    ==========================================================================================================
    */
    public function retornaAutorizacoes(){
        $autorizacoes = DB::connection('alfameta')->select(
            'SELECT * FROM AutorizacaoConsulta'
        );
        array_walk_recursive($autorizacoes, function(&$value, $key) {
            if (is_string($value)) {
                $value = iconv('windows-1252', 'utf-8', $value);
            }
        });
        return response()->json($autorizacoes);
    }
    /*
    ==========================================================================================================
    */
    public function testeInsert(Request $teste){
        $Teste = DB::connection('alfameta')->insert('INSERT INTO Cliente WITH AUTO NAME SELECT (SELECT max(codcliente) FROM Cliente)+1 AS codcliente, \''.$teste->nome.'\' AS Nome,\' '.$teste->endereco.'\' as Endereco ,  \'F\' AS TipoPessoa, \'F\' AS TipoCliente, \'F\' AS Risco ,\'S\' AS ativo');
        return \response()->json(['Insert OK']);
    }

    public function recebeGuia(Request $request){        
        $codcliente = $request->codcliente;
        $coddependente = $request->coddependente;
        $codmedico = $request->codmedico;
        $codclinica = $request->codclinica;
        $codespecialidade = $request->codespecialidade;
        $dataatendimento = $request->dataatendimento;
        $horaatendimento = $request->horaatendimento;
        $observacoes = $request->observacoes;
        $CodRequisicaoApp = $request->CodRequisicaoApp;
        try{
            $inadimplente = DB::connection('alfameta')->select('select codempresa,codcliente,sum(saldoreceber) as saldoreceber from ContaReceber where datavencimento < today(*) and STATUS in( \'A\',\'P\') AND codcliente = \''.$codcliente.'\' group by CodEmpresa,CodCliente');
            if(!$inadimplente){
                $insereRequisicao = DB::connection('alfameta')->insert('INSERT INTO AutorizacaoConsulta WITH AUTO NAME SELECT (SELECT max(CodAutorizacaoConsulta) FROM AutorizacaoConsulta)+1 AS CodAutorizacaoConsulta, 
                \''.$codcliente.'\' AS codcliente,
                \''.$coddependente.'\' as CodDependenteCliente ,
                \''.$codmedico.'\' as CodCadastroConveniado, 
                \''.$codclinica.'\' as CodCadastroClinica, 
                \''.$codespecialidade.'\' as CodCadastroServicoConveniado, 
                \''.$dataatendimento.'\' as Data, 
                \''.$horaatendimento.'\' as hora,
                \''.$observacoes.'\' as Observacao,
                \''.$CodRequisicaoApp.'\' as CodRequisicaoApp,
                \'1\' as codempresa,
                \'9\' as codresponsavel');
                
                $codRequisicao = DB::connection('alfameta')->select('SELECT  CodAutorizacaoConsulta from AutorizacaoConsulta where CodRequisicaoApp = \''.$CodRequisicaoApp.'\'');                                                
                return response()->json([$codRequisicao,'autorizado'=>true],200);
            }else{
                return response()->json(['autorizada'=>false],401);
            }
        }catch(\Exception $e){
            return response()->json(['erro'=>['msg'=>'Erro ao acessar o banco de dados, Por favor contate o suporte'.$e]],500);
        }       
    }
}


//SELECT rt.codEmpresa, rt.codrecebimento, rt.codcontareceber, r.datarecebimento, cr.codcliente, rt.valorrecebimentoliquido FROM DBA.RecebimentoTitulo  AS rt JOIN DBA.Recebimento AS R ON (rt.codempresa = r.codempresa AND rt.codrecebimento = r.codrecebimento) JOIN DBA.ContaReceber AS CR ON (rt.codempresa = CR.codempresa AND rt.codcontareceber = CR.codcontareceber) WHERE r.status = 'R'


//https://github.com/AdrianGomezCL/laravel-odbc-driver