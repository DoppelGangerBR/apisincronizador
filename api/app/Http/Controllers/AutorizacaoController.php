<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use JWTAuth;
use Hash;
use JWTGuard;
use Illuminate\Support\Facades\DB;

class AutorizacaoController extends Controller
{
    public function login(Request $request)
    {
        $usuarioRequest = $request->usuario;
        $senha = $request->senha;
        
        $usuario = Usuario::where('usuario', $usuarioRequest)->get();
        if(!$usuario){
            return response()->json([
                'Erro' => 'Usuario não encontrado'
            ], 401);            
        }               
        
        if(!Hash::check($senha, $usuario[0]->senha)){
            return response()->json([
                'Erro' => 'Senha invalida!'
            ], 401);
        }        
        $token = JWTAuth::fromUser($usuario[0]);
        $objectToken = JWTAuth::setToken($token);        
        $usuario->auth_token = $token;
        $insereToken = DB::connection('alfameta')->insert('UPDATE usuariosweb set auth_token =  \''.$token.'\' where usuario = \''.$usuarioRequest.'\'');        
        $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');

        return response()->json([
            'token_acesso' => $token,
            'tipo_token' => 'bearer',
            'expira_em' => '30 dias'
        ]);
    }
}
