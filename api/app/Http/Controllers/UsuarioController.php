<?php

namespace App\Http\Controllers;
use App\Usuario;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use JWTGuard;
use Illuminate\Support\Facades\DB;
class UsuarioController extends Controller
{
    /*public function index()
    {
        $usuario = Usuario::where('ativo', true)->orderBy('id', 'asc')->get();
        return response()->json($usuario);
    }*/

    //Cria novo usuario
    public function store(Request $request){        
        $request->senha = \Hash::make($request->senha); //criptografa a senha    
        //APÓS INSERIR O USUARIO, É PRECISO ADICIONAR UM ID MANUALMENTE NO USUARIO    
        $novoUsuario = DB::connection('alfameta')->insert(
            'INSERT INTO usuariosweb with auto name  select (SELECT max(id) FROM usuariosweb)+1 AS id, \''.$request->usuario.'\' as usuario, \''.$request->senha.'\' as senha'
        );
        $novoUsuario = DB::connection('alfameta')->select(
            'SELECT * FROM usuariosweb where usuario = \''.$request->usuario.'\' and senha = \''.$request->senha.'\''
        );
        if($novoUsuario){
            return response()->json(['msg'=>'Novo usuario criado com sucesso!', $novoUsuario]);
        }else{
            return response()->json(['msg'=>'Falha ao criar novo usuario']);
        }
        
    }

    /*public function update(Request $request, $codusuario){
        $dadosAlteracao = $request->all();
        if(array_key_exists('senha', $dadosAlteracao)){
            if($dadosAlteracao['senha'] != null || $dadosAlteracao['senha'] != ''){
                $dadosAlteracao['senha']  = \Hash::make($dadosAlteracao['senha']);
            }        
        }
        
        $Usuario = Usuario::where('id' ,'=', $codusuario)->first();
        try{
            $update = $Usuario->update($dadosAlteracao);
            if($update){
                return response()->json(['status'=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false, $e]);
        }
    }
    public function desativaUsuario(Request $request){
        $status = false;
        $dados = $request->all();
        $id = $dados['id'];
        try{
            $Usuario = Usuario::where('id', '=', $id);
            $desativa = $Usuario->update($dados);
            if($desativa)            {
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }*/

    public function guard()
    {
        return Auth::guard();
    }
}