<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//ROTAS PADRÕES DO LARAVEL
Route::get('/', function(){
    return response()->json(['message' => 'API Online', 'Status' => 'Connected']);;
});
Route::get('/', function(){
    return redirect('api');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
   
    
    
//ROTA RESPONSAVEL POR FAZER LOGIN DOS USUARIOS
Route::post('auth/login','AutorizacaoController@login');
//Route::post('auth/logout','AutorizacaoController@logout');
//Route::post('auth', 'AutorizacaoController@ValidaToken');  


//Tudo o que estiver dentro do grupo abaixo é protegido por token    
Route::group(['middleware' => ['jwt.verify']], function(){
    Route::get('clientes','SincronizacaoController@retornaClientes');
    Route::get('historico','SincronizacaoController@historicoPagamentoUsuario');
    Route::get('dependentes','SincronizacaoController@retornaDependentes');
    Route::get('clinicas','SincronizacaoController@retornaClinicas');
    Route::get('medicos','SincronizacaoController@retornaMedicos');
    Route::get('cidades','SincronizacaoController@retornaCidades');
    Route::get('servicos','SincronizacaoController@retornaServicos');
    Route::get('medicoxclinica','SincronizacaoController@retornaMedicoXClinica');
    Route::get('medicoxservico','SincronizacaoController@retornaMedicoXServico');
    Route::get('clinicaxservico','SincronizacaoController@retornaClinicaXServico');
    Route::get('autorizacoes','SincronizacaoController@retornaAutorizacoes');
    Route::post('guias','SincronizacaoController@recebeGuia');
});


 /*
Essa rota é para criar um novo usuario, ela é desprotegida
deve ser utilizada somente para criação do primeiro usuario (Administrador) ou após a criação 
da quantidade necessaria de usuarios necessarios.
Pra usa-la, basta descomentar ela, e após o uso, comentar novamente
*/
Route::post('Usuario', 'UsuarioController@store');